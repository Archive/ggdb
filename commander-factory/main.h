/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef MAIN_H
#define MAIN_H 1

#include <gnome.h>
#include <config.h>
#include <libgnorba/gnorba.h>

#include <gnome-debug/gnome-debug.h>

/*** App-specific servant structures ***/

typedef struct {
    POA_GNOME_Debugger_Program servant;
    PortableServer_POA poa;

    CosTypedEventChannelAdmin_TypedSupplierAdmin typed_supplier_admin;
    CosTypedEventChannelAdmin_TypedConsumerAdmin typed_consumer_admin;
    CosTypedEventChannelAdmin_TypedEventChannel typed_event_channel;
    CosTypedEventChannelAdmin_TypedProxyPushConsumer typed_proxy_push_consumer;

    GNOME_Debugger_ProgramEvents program_events_object;

    int refcount;
} impl_POA_GNOME_Debugger_Program;

typedef struct {
    POA_GNOME_Debugger_ProgramFactory servant;
    PortableServer_POA poa;

    int refcount;
} impl_POA_GNOME_Debugger_ProgramFactory;

#endif /* MAIN_H */
