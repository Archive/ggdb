/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <main.h>
#include <gnome-debug/gnome-debug.h>

extern const struct CORBA_TypeCode_struct TC_GNOME_Debugger_ProgramEvents_struct;
extern CORBA_boolean
CORBA_Object_is_a (CORBA_Object, CORBA_char *, CORBA_Environment *);

/*** Implementation stub prototypes ***/

static void 
impl_GNOME_Debugger_Program__destroy (impl_POA_GNOME_Debugger_Program *servant,
                                      CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_attach (impl_POA_GNOME_Debugger_Program *servant,
                                    CORBA_short pid, CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_execute (impl_POA_GNOME_Debugger_Program *servant,
                                     GNOME_Debugger_arg_list *args,
                                     CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_load_corefile (impl_POA_GNOME_Debugger_Program *servant,
                                           CORBA_char *corefile_name,
                                           CORBA_Environment *ev);

static GNOME_Debugger_source_file_list *
impl_GNOME_Debugger_Program_get_sources (impl_POA_GNOME_Debugger_Program *servant, 
                                         CORBA_Environment * ev);

static void
impl_GNOME_Debugger_Program_set_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                            CORBA_char *file_name,
                                            CORBA_long line_num,
                                            CORBA_char *condition,
                                            CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_set_breakpoint_function (impl_POA_GNOME_Debugger_Program *servant,
                                                     CORBA_char *function_name,
                                                     CORBA_char *condition,
                                                     CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_enable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_disable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                                CORBA_long bp_num,
                                                CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_delete_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_ref (impl_POA_GNOME_Debugger_Program * servant,
                                 CORBA_Environment * ev);
static void
impl_GNOME_Debugger_Program_unref (impl_POA_GNOME_Debugger_Program * servant,
                                   CORBA_Environment * ev);
static CORBA_Object
impl_GNOME_Debugger_Program_query_interface (impl_POA_GNOME_Debugger_Program * servant,
                                             CORBA_char * repoid,
                                             CORBA_Environment * ev);

/*** epv structures ***/

static PortableServer_ServantBase__epv impl_GNOME_Debugger_Program_base_epv =
{
    NULL,                       /* _private data */
    NULL,                       /* finalize routine */
    NULL,                       /* default_POA routine */
};
static POA_GNOME_Debugger_Program__epv impl_GNOME_Debugger_Program_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_Program_attach,
    (gpointer) & impl_GNOME_Debugger_Program_execute,
    (gpointer) & impl_GNOME_Debugger_Program_load_corefile,
    (gpointer) & impl_GNOME_Debugger_Program_get_sources,
    (gpointer) & impl_GNOME_Debugger_Program_set_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_set_breakpoint_function,
    (gpointer) & impl_GNOME_Debugger_Program_enable_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_disable_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_delete_breakpoint,
};
static POA_GNOME_Unknown__epv impl_GNOME_Debugger_Program_GNOME_Unknown_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_Program_ref,
    (gpointer) & impl_GNOME_Debugger_Program_unref,
    (gpointer) & impl_GNOME_Debugger_Program_query_interface,
};

/*** vepv structures ***/

static POA_GNOME_Debugger_Program__vepv impl_GNOME_Debugger_Program_vepv =
{
    &impl_GNOME_Debugger_Program_base_epv,
    &impl_GNOME_Debugger_Program_GNOME_Unknown_epv,
    &impl_GNOME_Debugger_Program_epv,
};

/*** Stub implementations ***/

static GNOME_Debugger_Program 
impl_GNOME_Debugger_Program__create (PortableServer_POA poa,
                                     CORBA_Environment * ev)
{
    GNOME_Debugger_Program retval;
    impl_POA_GNOME_Debugger_Program *newservant;
    PortableServer_ObjectId *objid;
    CORBA_Object typed_consumer;
    CORBA_boolean correct;
    const char *repo_id;

    newservant = g_new0 (impl_POA_GNOME_Debugger_Program, 1);
    newservant->servant.vepv = &impl_GNOME_Debugger_Program_vepv;
    newservant->refcount = 1;
    newservant->poa = poa;

    POA_GNOME_Debugger_Program__init ((PortableServer_Servant) newservant, ev);
    objid = PortableServer_POA_activate_object (poa, newservant, ev);
    CORBA_free (objid);
    retval = PortableServer_POA_servant_to_reference (poa, newservant, ev);

    fprintf (stderr, "impl_GNOME_Debugger_Program__create (%d): %p\n",
	     getpid (), retval);

    newservant->typed_event_channel = 
	goad_server_activate_with_id (NULL,
				      "gnome_debugger_typed_event_channel",
				      0,
				      NULL);

    fprintf (stderr, "Got typed event channel %p\n",
             newservant->typed_event_channel);

    newservant->typed_supplier_admin =
        CosTypedEventChannelAdmin_TypedEventChannel_for_suppliers
        (newservant->typed_event_channel, ev);

    fprintf (stderr, "Got typed supplier admin %p\n",
             newservant->typed_supplier_admin);

    newservant->typed_consumer_admin =
        CosTypedEventChannelAdmin_TypedEventChannel_for_consumers
        (newservant->typed_event_channel, ev);

    fprintf (stderr, "Got typed consumer admin %p\n",
             newservant->typed_consumer_admin);

    newservant->typed_proxy_push_consumer =
        CosTypedEventChannelAdmin_TypedSupplierAdmin_obtain_typed_push_consumer
        (newservant->typed_supplier_admin, "program-events", ev);

    fprintf (stderr, "Got typed proxy push consumer %p\n",
             newservant->typed_proxy_push_consumer);

    typed_consumer =
        CosTypedEventChannelAdmin_TypedProxyPushConsumer_get_typed_consumer
        (newservant->typed_proxy_push_consumer, ev);

    fprintf (stderr, "Got typed push consumer %p\n", typed_consumer);

    repo_id = CORBA_TypeCode_id (&TC_GNOME_Debugger_ProgramEvents_struct, ev);
    
    correct = CORBA_Object_is_a (typed_consumer, repo_id, ev);

    fprintf (stderr, "CORBA_Object_is_a (%p,%s) = %d\n",
             typed_consumer, repo_id, correct);

    if (correct)
        newservant->program_events_object =
            (GNOME_Debugger_ProgramEvents) typed_consumer;

    return retval;
}

static void 
impl_GNOME_Debugger_Program__destroy (impl_POA_GNOME_Debugger_Program *servant,
                                      CORBA_Environment *ev)
{
    PortableServer_ObjectId *objid;

    objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
    PortableServer_POA_deactivate_object (servant->poa, objid, ev);
    CORBA_free (objid);
        
    POA_GNOME_Debugger_Program__fini ((PortableServer_Servant) servant,
                                      ev);
    g_free (servant);
}

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_attach (impl_POA_GNOME_Debugger_Program *servant,
                                    CORBA_short pid, CORBA_Environment *ev)
{
    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_execute (impl_POA_GNOME_Debugger_Program *servant,
                                     GNOME_Debugger_arg_list *args,
                                     CORBA_Environment *ev)
{
    GNOME_Debugger_ProgramEvents_execute (servant->program_events_object,
                                          1, args, ev);

    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_load_corefile (impl_POA_GNOME_Debugger_Program *servant,
                                           CORBA_char *corefile_name,
                                           CORBA_Environment *ev)
{
    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_source_file_list *
impl_GNOME_Debugger_Program_get_sources (impl_POA_GNOME_Debugger_Program *servant, 
                                         CORBA_Environment * ev)
{
    GNOME_Debugger_source_file_list *retval;
    int sources_len = 0;
    
    retval = GNOME_Debugger_source_file_list__alloc ();
    retval->_buffer = 
        CORBA_sequence_CORBA_string_allocbuf (sources_len);
    retval->_length = sources_len;

    return retval;
}

static void
impl_GNOME_Debugger_Program_set_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                            CORBA_char *file_name,
                                            CORBA_long line_num,
                                            CORBA_char *condition,
                                            CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_set_breakpoint_function (impl_POA_GNOME_Debugger_Program *servant,
                                                     CORBA_char *function_name,
                                                     CORBA_char *condition,
                                                     CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_enable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_disable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                                CORBA_long bp_num,
                                                CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_delete_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_ref (impl_POA_GNOME_Debugger_Program * servant,
                                 CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_ref ()\n");
    
    servant->refcount++;
}

static void
impl_GNOME_Debugger_Program_unref (impl_POA_GNOME_Debugger_Program * servant,
                                   CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_unref ()\n");

    servant->refcount--;
    /* FIXME: Destroy if recount == 0 */
    if (servant->refcount == 0) {
        impl_GNOME_Debugger_Program__destroy (servant, ev);

        gtk_main_quit ();
    }
}

static CORBA_Object
impl_GNOME_Debugger_Program_query_interface (impl_POA_GNOME_Debugger_Program * servant,
                                             CORBA_char * repoid,
                                             CORBA_Environment * ev)
{
    CORBA_Object retval = CORBA_OBJECT_NIL;
    
    fprintf (stderr, "impl_GNOME_Debugger_Program_query_interface ()\n");

    return retval;
}

