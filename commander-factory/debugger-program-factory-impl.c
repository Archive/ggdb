/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */

#include "debugger-program-impl.c"

/*** Implementation stub prototypes ***/

static void 
impl_GNOME_Debugger_ProgramFactory__destroy (impl_POA_GNOME_Debugger_ProgramFactory *servant, CORBA_Environment *ev);

static CORBA_Object
impl_GNOME_Debugger_ProgramFactory_create (impl_POA_GNOME_Debugger_ProgramFactory *servant, GNOME_Debugger_type_string debugger_type, GNOME_Debugger_arg_list * backend_arguments, CORBA_Environment * ev);

static CORBA_boolean
impl_GNOME_Debugger_ProgramFactory_supports (impl_POA_GNOME_Debugger_ProgramFactory *servant, const CORBA_char *obj_goad_id, CORBA_Environment *ev);

static CORBA_Object
impl_GNOME_Debugger_ProgramFactory_create_object (impl_POA_GNOME_Debugger_ProgramFactory *servant, const CORBA_char *obj_goad_id, const GNOME_stringlist *params, CORBA_Environment *ev);

static void
impl_GNOME_Debugger_ProgramFactory_ref (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_Environment * ev);

static void
impl_GNOME_Debugger_ProgramFactory_unref (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_Environment * ev);

static CORBA_Object
impl_GNOME_Debugger_ProgramFactory_query_interface (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_char * repoid, CORBA_Environment * ev);

/*** epv structures ***/

static PortableServer_ServantBase__epv impl_GNOME_Debugger_ProgramFactory_base_epv =
{
    NULL,                       /* _private data */
    NULL,                       /* finalize routine */
    NULL,                       /* default_POA routine */
};
static POA_GNOME_Debugger_ProgramFactory__epv impl_GNOME_Debugger_ProgramFactory_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_create,
};
static POA_GNOME_GenericFactory__epv impl_GNOME_Debugger_ProgramFactory_GNOME_GenericFactory_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_supports,
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_create_object,
};
static POA_GNOME_Unknown__epv impl_GNOME_Debugger_ProgramFactory_GNOME_Unknown_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_ref,
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_unref,
    (gpointer) & impl_GNOME_Debugger_ProgramFactory_query_interface,
};

/*** vepv structures ***/

static POA_GNOME_Debugger_ProgramFactory__vepv impl_GNOME_Debugger_ProgramFactory_vepv =
{
    &impl_GNOME_Debugger_ProgramFactory_base_epv,
    &impl_GNOME_Debugger_ProgramFactory_GNOME_GenericFactory_epv,
    &impl_GNOME_Debugger_ProgramFactory_GNOME_Unknown_epv,
    &impl_GNOME_Debugger_ProgramFactory_epv,
};

/*** Stub implementations ***/

static GNOME_Debugger_ProgramFactory 
impl_GNOME_Debugger_ProgramFactory__create (PortableServer_POA poa,
                                              CORBA_Environment * ev)
{
    GNOME_Debugger_ProgramFactory retval;
    impl_POA_GNOME_Debugger_ProgramFactory *newservant;
    PortableServer_ObjectId *objid;

    newservant = g_new0 (impl_POA_GNOME_Debugger_ProgramFactory, 1);
    newservant->servant.vepv = &impl_GNOME_Debugger_ProgramFactory_vepv;
    newservant->poa = poa;

    POA_GNOME_Debugger_ProgramFactory__init ((PortableServer_Servant) newservant, ev);
    objid = PortableServer_POA_activate_object (poa, newservant, ev);
    CORBA_free (objid);
    retval = PortableServer_POA_servant_to_reference (poa, newservant, ev);

    return retval;
}


static void
impl_GNOME_Debugger_ProgramFactory__destroy (impl_POA_GNOME_Debugger_ProgramFactory *servant, CORBA_Environment *ev)
{
    PortableServer_ObjectId *objid;

    objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
    PortableServer_POA_deactivate_object (servant->poa, objid, ev);
    CORBA_free (objid);
        
    POA_GNOME_Debugger_ProgramFactory__fini ((PortableServer_Servant) servant,
                                             ev);
    g_free (servant);
}

static CORBA_Object 
impl_GNOME_Debugger_ProgramFactory_create (impl_POA_GNOME_Debugger_ProgramFactory *servant, GNOME_Debugger_type_string debugger_type, GNOME_Debugger_arg_list * backend_arguments, CORBA_Environment * ev)
{
#if 0
    CORBA_Object retval = CORBA_OBJECT_NIL;
    const char *params [] = { "-n", "--batch", "--guile-eval",
			      "(use-modules (gdb corba)) (gdb-corba-main)",
			      NULL };
    GoadServer sinfo;
    
    fprintf (stderr, "impl_GNOME_Debugger_ProgramFactory_create ()\n");
    
    memset (&sinfo, 0, sizeof (GoadServer));
    
    sinfo.type = GOAD_SERVER_EXE;
    sinfo.flags = GOAD_ACTIVATE_NEW_ONLY;
    sinfo.location_info = "gdb-guile";
    
    retval = goad_server_activate (&sinfo,
				   GOAD_ACTIVATE_REMOTE |
				   GOAD_ACTIVATE_NEW_ONLY,
				   params);

    fprintf (stderr, "impl_GNOME_Debugger_ProgramFactory_create  = %p\n",
             retval);

    return retval;
#else
    CORBA_Object retval;

    retval = impl_GNOME_Debugger_Program__create (servant->poa, ev);

    return retval;
#endif
}


static CORBA_boolean
impl_GNOME_Debugger_ProgramFactory_supports (impl_POA_GNOME_Debugger_ProgramFactory *servant, const CORBA_char *obj_goad_id, CORBA_Environment *ev)
{
    if (!strcmp (obj_goad_id, "gnome_debugger_program"))
        return TRUE;
    else
        return FALSE;
}

static CORBA_Object
impl_GNOME_Debugger_ProgramFactory_create_object (impl_POA_GNOME_Debugger_ProgramFactory *servant, const CORBA_char *obj_goad_id, const GNOME_stringlist *params, CORBA_Environment *ev)
{
    GNOME_Debugger_arg_list *backend_args = NULL;
    GNOME_Debugger_type_string type_string = NULL;
    CORBA_Object retval = CORBA_OBJECT_NIL;
    int i;

    if (strcmp (obj_goad_id, "gnome_debugger_program_gdb_guile")) 
        goto error_out;
    
    if (params->_length < 1)
        goto error_out;
    
    type_string = CORBA_string_dup (params->_buffer [0]);
  
    backend_args = GNOME_Debugger_arg_list__alloc ();
   
    backend_args->_buffer = 
        CORBA_sequence_GNOME_Debugger_Istring_allocbuf (params->_length-1);
    backend_args->_length = backend_args->_maximum = params->_length-1;
   
    for (i = 0; i < params->_length-1; i++)
        backend_args->_buffer [i] = CORBA_string_dup (params->_buffer [i+1]);
    
    retval = impl_GNOME_Debugger_ProgramFactory_create
        (servant, type_string, backend_args, ev);
    
    if (ev->_major == CORBA_NO_EXCEPTION)
        goto out;
    
 error_out:
    CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
                         ex_GNOME_GenericFactory_CannotActivate,
                         NULL);
    
 out:
    if (type_string)
        CORBA_free (type_string);
    
    if (backend_args)
        GNOME_Debugger_arg_list__free (backend_args, NULL, 1);

    return retval;
}

static void
impl_GNOME_Debugger_ProgramFactory_ref (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_ProgramFactory_ref ()\n");
    
    servant->refcount++;
}

static void
impl_GNOME_Debugger_ProgramFactory_unref (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_ProgramFactory_unref ()\n");

    servant->refcount--;
    if (servant->refcount == 0) {
        impl_GNOME_Debugger_ProgramFactory__destroy (servant, ev);

        gtk_main_quit ();
    }
}

static CORBA_Object
impl_GNOME_Debugger_ProgramFactory_query_interface (impl_POA_GNOME_Debugger_ProgramFactory * servant, CORBA_char * repoid, CORBA_Environment * ev)
{
    CORBA_Object retval = CORBA_OBJECT_NIL;
    
    fprintf (stderr, "impl_GNOME_Debugger_ProgramFactory_query_interface ()\n");

    return retval;
}

