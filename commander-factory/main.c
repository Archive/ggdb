/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include "main.h"
#include "debugger-program-factory-impl.c"

int
main (int argc, char *argv [])
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    CORBA_Object server, name_service;
    
    PortableServer_POA root_poa;
    PortableServer_POAManager pm;

    /* Setup CORBA */
    CORBA_exception_init (&ev);
    
    orb = gnome_CORBA_init (PACKAGE, VERSION, &argc, argv,
			    GNORBA_INIT_SERVER_FUNC, &ev);

    root_poa = 
        (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb,
                                                                   "RootPOA",
                                                                   &ev);
    

    server = impl_GNOME_Debugger_ProgramFactory__create (root_poa, &ev);
    pm = PortableServer_POA__get_the_POAManager (root_poa, &ev);
    PortableServer_POAManager_activate (pm, &ev);

    name_service = gnome_name_service_get ();
    goad_server_register (name_service, 
                          server,
                          "gnome_debugger_program_gdb_guile_factory",
                          "object",
                          &ev);
    
    CORBA_exception_free (&ev);

    gtk_main ();

    goad_server_unregister (name_service,
                            "gnome_debugger_program_gdb_guile_factory",
                            "object",
                            &ev);
    return 0;
}





