/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#ifndef GDBGUILE_CORBA_H
#define GDBGUILE_CORBA_H 1

#include <gnome.h>
#include <config.h>
#include <libgnorba/gnorba.h>

#include <guile/gh.h>
#include <libguile/dynl.h>
#include <libguile/snarf.h>
#include <libguile/modules.h>
#include <libguile/dynwind.h>
#include <libguile/strports.h>

#include <gnome-debug/gnome-debug.h>

/*** App-specific servant structures ***/

typedef struct {
    POA_GNOME_Debugger_Program servant;
    PortableServer_POA poa;

    CORBA_Object event_channel_objref;

    int refcount;
} impl_POA_GNOME_Debugger_Program;

typedef struct {
    CORBA_Environment *ev;
    char *function;
    char *file;
    long line;
} sgchbe_data;

/* gdb_corba_data */

typedef struct {
    impl_POA_GNOME_Debugger_Program *servant;
    CORBA_Environment *ev;
} ggdb_corba_data;

#define SCM_GGDB_CORBA(obj) ((ggdb_corba_data *) SCM_CDR (obj))
#define SCM_GGDB_CORBA_SERVANT(obj) (SCM_GGDB_CORBA (obj)->servant)
#define SCM_GGDB_CORBA_ENV(obj) (SCM_GGDB_CORBA (obj)->ev)
#define SCM_GGDB_CORBAP(obj) (SCM_CAR (obj) == tc16_ggdb_corba_data)

extern long tc16_ggdb_corba_data;

/* gdbguile-corba.c */
void scm_init_gdb_corba_module (void);
void scm_debug_print (SCM obj);

/* stack.c */
void gdb_corba_get_backtrace (impl_POA_GNOME_Debugger_Program *servant,
                              GNOME_Debugger_Stack *stack,
                              CORBA_Environment *ev);

/* guile.c - global variables */
extern SCM scm_ggdb_guile_module_ref;
extern SCM scm_ggdb_guile_gdb_corba;
extern SCM scm_ggdb_guile_gdb_corba_module;

extern SCM scm_ggdb_guile_gdb_corba_run;
extern SCM scm_ggdb_guile_gdb_corba_backtrace;

extern SCM scm_ggdb_guile_apply_data;
extern SCM scm_ggdb_guile_apply_stack;

/* guile.c */
GList *
glist_string_from_scm (SCM guile_list);

#define scm_gdb_corba_guile_apply(servant,proc,arg1,args,ev) _scm_gdb_corba_guile_apply(servant,proc,arg1,args,ev,__FUNCTION__,__FILE__,__LINE__)

SCM
_scm_gdb_corba_guile_apply (impl_POA_GNOME_Debugger_Program *servant,
                            SCM proc, SCM arg1, SCM args,
                            CORBA_Environment *ev,
                            char *function, char *file, unsigned long line);

/* Initializations */
void scm_init_gdb_corba_module_guile (void);
void scm_init_gdb_corba_module_callbacks (void);

void _initialize_guile (void);
void _initialize_stack (void);
void _initialize_callbacks (void);

#endif /* GDBGUILE_CORBA_H */
