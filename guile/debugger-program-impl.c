/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <gdbguile-corba.h>

/*** Implementation stub prototypes ***/

static void 
impl_GNOME_Debugger_Program__destroy (impl_POA_GNOME_Debugger_Program *servant,
                                      CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_attach (impl_POA_GNOME_Debugger_Program *servant,
                                    CORBA_short pid, CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_execute (impl_POA_GNOME_Debugger_Program *servant,
                                     GNOME_Debugger_arg_list args,
                                     CORBA_Environment *ev);

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_load_corefile (impl_POA_GNOME_Debugger_Program *servant,
                                           CORBA_char *corefile_name,
                                           CORBA_Environment *ev);

static GNOME_Debugger_source_file_list *
impl_GNOME_Debugger_Program_get_sources (impl_POA_GNOME_Debugger_Program *servant, 
                                         CORBA_Environment * ev);

static void
impl_GNOME_Debugger_Program_set_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                            CORBA_char *file_name,
                                            CORBA_long line_num,
                                            CORBA_char *condition,
                                            CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_set_breakpoint_function (impl_POA_GNOME_Debugger_Program *servant,
                                                     CORBA_char *function_name,
                                                     CORBA_char *condition,
                                                     CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_enable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_disable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                                CORBA_long bp_num,
                                                CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_delete_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev);

static void
impl_GNOME_Debugger_Program_ref (impl_POA_GNOME_Debugger_Program * servant,
                                 CORBA_Environment * ev);
static void
impl_GNOME_Debugger_Program_unref (impl_POA_GNOME_Debugger_Program * servant,
                                   CORBA_Environment * ev);
static CORBA_Object
impl_GNOME_Debugger_Program_query_interface (impl_POA_GNOME_Debugger_Program * servant,
                                             CORBA_char * repoid,
                                             CORBA_Environment * ev);

/*** epv structures ***/

static PortableServer_ServantBase__epv impl_GNOME_Debugger_Program_base_epv =
{
    NULL,                       /* _private data */
    NULL,                       /* finalize routine */
    NULL,                       /* default_POA routine */
};
static POA_GNOME_Debugger_Program__epv impl_GNOME_Debugger_Program_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_Program_attach,
    (gpointer) & impl_GNOME_Debugger_Program_execute,
    (gpointer) & impl_GNOME_Debugger_Program_load_corefile,
    (gpointer) & impl_GNOME_Debugger_Program_get_sources,
    (gpointer) & impl_GNOME_Debugger_Program_set_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_set_breakpoint_function,
    (gpointer) & impl_GNOME_Debugger_Program_enable_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_disable_breakpoint,
    (gpointer) & impl_GNOME_Debugger_Program_delete_breakpoint,
};
static POA_GNOME_Unknown__epv impl_GNOME_Debugger_Program_GNOME_Unknown_epv =
{
    NULL,                       /* _private */
    (gpointer) & impl_GNOME_Debugger_Program_ref,
    (gpointer) & impl_GNOME_Debugger_Program_unref,
    (gpointer) & impl_GNOME_Debugger_Program_query_interface,
};

/*** vepv structures ***/

static POA_GNOME_Debugger_Program__vepv impl_GNOME_Debugger_Program_vepv =
{
    &impl_GNOME_Debugger_Program_base_epv,
    &impl_GNOME_Debugger_Program_GNOME_Unknown_epv,
    &impl_GNOME_Debugger_Program_epv,
};

/*** Stub implementations ***/

static GNOME_Debugger_Program 
impl_GNOME_Debugger_Program__create (PortableServer_POA poa,
                                     CORBA_Environment * ev)
{
    GNOME_Debugger_Program retval;
    impl_POA_GNOME_Debugger_Program *newservant;
    PortableServer_ObjectId *objid;

    newservant = g_new0 (impl_POA_GNOME_Debugger_Program, 1);
    newservant->servant.vepv = &impl_GNOME_Debugger_Program_vepv;
    newservant->refcount = 1;
    newservant->poa = poa;

    newservant->event_channel_objref = 
	goad_server_activate_with_id (NULL,
				      "gnome_debugger_typed_event_channel",
				      0,
				      NULL);

    POA_GNOME_Debugger_Program__init ((PortableServer_Servant) newservant, ev);
    objid = PortableServer_POA_activate_object (poa, newservant, ev);
    CORBA_free (objid);
    retval = PortableServer_POA_servant_to_reference (poa, newservant, ev);

    fprintf (stderr, "impl_GNOME_Debugger_Program__create (%d): %p\n",
	     getpid (), retval);

    return retval;
}

static void 
impl_GNOME_Debugger_Program__destroy (impl_POA_GNOME_Debugger_Program *servant,
                                        CORBA_Environment *ev)
{
    PortableServer_ObjectId *objid;

    objid = PortableServer_POA_servant_to_id (servant->poa, servant, ev);
    PortableServer_POA_deactivate_object (servant->poa, objid, ev);
    CORBA_free (objid);
        
    POA_GNOME_Debugger_Program__fini ((PortableServer_Servant) servant,
                                        ev);
    g_free (servant);
}

#if 0

static void
impl_GNOME_Debugger_Program_load_binary (impl_POA_GNOME_Debugger_Program *servant,
                                         CORBA_char *binary_name,
                                         CORBA_Environment *ev)
{
    gchar *command = g_strdup_printf ("(gdb-corba-file \"%s\")", binary_name);

    fprintf (stderr, "impl_GNOME_Debugger_Program_load_binary (%d,%p) - %s - '%s'\n",
	     getpid (), servant, binary_name, command);

    scm_debug_print (gh_eval_str (command));
    g_free (command);

    scm_debug_print (gh_eval_str ("(gdb-corba-break-function \"main\")"));
}

static GNOME_Debugger_source_file_list *
impl_GNOME_Debugger_Program_get_sources (impl_POA_GNOME_Debugger_Program *servant, 
                                         CORBA_Environment * ev)
{
    SCM source_list;
    GList *sources;
    GList *iter;
    GNOME_Debugger_source_file_list *retval;
    int sources_len;
    int i;

    gchar *command = "(gdb-sources)";
    
    fprintf (stderr, "impl_GNOME_Debugger_Program_get_sources () - '%s'\n",
             command);

    source_list = gh_eval_str (command);
    sources = glist_string_from_scm (source_list);
    sources_len = g_list_length (sources);
    
    retval = GNOME_Debugger_source_file_list__alloc ();
    retval->_buffer = 
        CORBA_sequence_CORBA_string_allocbuf (sources_len);
    retval->_length = sources_len;

    for (iter = sources, i = 0; iter != NULL; iter = iter->next, i++) {
        retval->_buffer[i] = CORBA_string_dup ((char*)iter->data);
    }

    for (iter = sources; iter != NULL; iter = iter->next) {
        free (iter->data);
        g_list_free_1 (iter);
    }

    return retval;
}

static void
impl_GNOME_Debugger_Program_execute (impl_POA_GNOME_Debugger_Program *servant,
                                     CORBA_char *args,
                                     CORBA_Environment *ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_execute (%d,%p) - '%s'\n",
	     getpid (), servant, args);

    scm_gdb_corba_guile_apply (servant,
                               scm_ggdb_guile_gdb_corba_run,
                               SCM_LIST1 (scm_makfrom0str (args)),
                               SCM_EOL,
                               ev);
}

static void
impl_GNOME_Debugger_Program_step_over (impl_POA_GNOME_Debugger_Program *servant,
                                       CORBA_Environment *ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_step_over (%d,%p)\n",
	     getpid (), servant);

    scm_debug_print (gh_eval_str ("(gdb-corba-next)"));
}

static void
impl_GNOME_Debugger_Program_step_into (impl_POA_GNOME_Debugger_Program *servant,
                                       CORBA_Environment *ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_step_into (%d,%p)\n",
	     getpid (), servant);

    scm_debug_print (gh_eval_str ("(gdb-corba-step)"));
}

static void
impl_GNOME_Debugger_Program_step_out (impl_POA_GNOME_Debugger_Program *servant,
                                      CORBA_Environment *ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_step_out (%d,%p)\n",
	     getpid (), servant);

    scm_debug_print (gh_eval_str ("(gdb-corba-finish)"));
}

static void
impl_GNOME_Debugger_Program_set_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                            CORBA_char *file_name,
                                            CORBA_long line_num,
                                            CORBA_char *condition,
                                            CORBA_Environment *ev)
{
    gchar *command = g_strdup_printf ("(gdb-corba-break \"%s\" %d)",
				      file_name, line_num);

    fprintf (stderr, "impl_GNOME_Debugger_Program_set_breakpoint (%d,%p) - '%s'\n",
	     getpid (), servant, command);

    scm_debug_print (gh_eval_str (command));
    g_free (command);
}

static void
impl_GNOME_Debugger_Program_set_breakpoint_function (impl_POA_GNOME_Debugger_Program *servant,
                                                     CORBA_char *function_name,
                                                     CORBA_char *condition,
                                                     CORBA_Environment *ev)
{
    gchar *command = g_strdup_printf ("(gdb-corba-break-function \"%s\")",
				      function_name);

    fprintf (stderr, "impl_GNOME_Debugger_Program_set_breakpoint_function "
	     "(%d,%p) - '%s'\n", getpid (), servant, command);

    scm_debug_print (gh_eval_str (command));
    g_free (command);
}

static GNOME_Debugger_StackFrame *
impl_GNOME_Debugger_Program_get_frame (impl_POA_GNOME_Debugger_Program *servant,
                                       CORBA_long id, CORBA_Environment * ev)
{
    GNOME_Debugger_StackFrame *frame;

    frame = GNOME_Debugger_StackFrame__alloc ();

    frame->id = id;
    frame->function = "Test_Function";
    
    frame->location.file.filename = "test.c";
    frame->location.file.is_canonical = FALSE;
    frame->location.line = 88;

    frame->virtual_address = 0;
    frame->intraline = TRUE;

    return frame;
}

static GNOME_Debugger_Stack *
impl_GNOME_Debugger_Program_get_backtrace (impl_POA_GNOME_Debugger_Program *servant,
                                           CORBA_Environment * ev)
{
    GNOME_Debugger_Stack *stack;
 
    stack = GNOME_Debugger_Stack__alloc ();
    stack->_length = stack->_maximum = 0;
    stack->_buffer = NULL;

    gdb_corba_get_backtrace (servant, stack, ev);

    return stack;
}

#endif

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_attach (impl_POA_GNOME_Debugger_Program *servant,
                                    CORBA_short pid, CORBA_Environment *ev)
{
    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_execute (impl_POA_GNOME_Debugger_Program *servant,
                                     GNOME_Debugger_arg_list args,
                                     CORBA_Environment *ev)
{
    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_Process
impl_GNOME_Debugger_Program_load_corefile (impl_POA_GNOME_Debugger_Program *servant,
                                           CORBA_char *corefile_name,
                                           CORBA_Environment *ev)
{
    return CORBA_OBJECT_NIL;
}

static GNOME_Debugger_source_file_list *
impl_GNOME_Debugger_Program_get_sources (impl_POA_GNOME_Debugger_Program *servant, 
                                         CORBA_Environment * ev)
{
    GNOME_Debugger_source_file_list *retval;
    int sources_len = 0;
    
    retval = GNOME_Debugger_source_file_list__alloc ();
    retval->_buffer = 
        CORBA_sequence_CORBA_string_allocbuf (sources_len);
    retval->_length = sources_len;

    return retval;
}

static void
impl_GNOME_Debugger_Program_set_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                            CORBA_char *file_name,
                                            CORBA_long line_num,
                                            CORBA_char *condition,
                                            CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_set_breakpoint_function (impl_POA_GNOME_Debugger_Program *servant,
                                                     CORBA_char *function_name,
                                                     CORBA_char *condition,
                                                     CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_enable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_disable_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                                CORBA_long bp_num,
                                                CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_delete_breakpoint (impl_POA_GNOME_Debugger_Program *servant,
                                               CORBA_long bp_num,
                                               CORBA_Environment *ev)
{
}

static void
impl_GNOME_Debugger_Program_ref (impl_POA_GNOME_Debugger_Program * servant,
                                 CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_ref ()\n");
    
    servant->refcount++;
}

static void
impl_GNOME_Debugger_Program_unref (impl_POA_GNOME_Debugger_Program * servant,
                                   CORBA_Environment * ev)
{
    fprintf (stderr, "impl_GNOME_Debugger_Program_unref ()\n");

    servant->refcount--;
    /* FIXME: Destroy if recount == 0 */
    if (servant->refcount == 0) {
        impl_GNOME_Debugger_Program__destroy (servant, ev);

        gtk_main_quit ();
    }
}

static CORBA_Object
impl_GNOME_Debugger_Program_query_interface (impl_POA_GNOME_Debugger_Program * servant,
                                             CORBA_char * repoid,
                                             CORBA_Environment * ev)
{
    CORBA_Object retval = CORBA_OBJECT_NIL;
    
    fprintf (stderr, "impl_GNOME_Debugger_Program_query_interface ()\n");

    return retval;
}

