(define-module (gdb corba)
  :use-module (gdb gdb)
  :use-module (gdb callbacks)
  :use-module (gtk dynlink)
  :use-module (gnome gnorba))

(gnome-corba-init "gdb-guile" "0.01"
		  '("gdb-guile" "--activate-goad-server" "debugger-commander")
		  '(init-server-func))

(merge-compiled-code "scm_init_gdb_corba_module" "libgdbguile-corba-module")

(define-public gdb-corba-callbacks
  (cons #t '((annotate-frames-invalid . (lambda ()
					  (gdb-ccb-frames-invalid)
					  )
				      )
	     (annotate-frame-end . (lambda ()
				     (gdb-ccb-frame-end)
				     )
				 )
	     )
	)
  )

(defmacro gdb-corba-catch-exception (expr)
  `(catch #t (lambda ()
	       (begin
		 (gdb-call-with-callback-vector gdb-corba-callbacks ,expr)
		 #t))
          (lambda args args)))

(define-public (gdb-corba-file filename)
  (gdb-corba-catch-exception (gdb-file filename)))

(define-public (gdb-corba-run args)
  (gdb-corba-catch-exception (gdb-run args)))

(define-public (gdb-corba-next)
  (gdb-corba-catch-exception (gdb-next)))

(define-public (gdb-corba-step)
  (gdb-corba-catch-exception (gdb-step)))

(define-public (gdb-corba-finish)
  (gdb-corba-catch-exception (gdb-finish)))

(define-public (gdb-corba-break file line)
  (let ((args (string-append file ":" (number->string line))))
    (gdb-corba-catch-exception (gdb-break args))))

(define-public (gdb-corba-break-function function)
  (gdb-corba-catch-exception (gdb-break function)))

(define-public (gdb-corba-frame)
  (gdb-corba-catch-exception (gdb-frame)))

(define-public (gdb-corba-backtrace)
  (gdb-backtrace))

