/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <gdbguile-corba.h>

static SCM scm_gdb_corba_module_ref = SCM_UNDEFINED;
static SCM scm_gdb_corba_module_set_x = SCM_UNDEFINED;
static SCM scm_gdb_corba_record_accessor = SCM_UNDEFINED;

static SCM scm_gdb_corba_guile_user = SCM_UNDEFINED;
static SCM scm_gdb_corba_guile_user_module = SCM_UNDEFINED;

static SCM scm_gdb_corba_gdb_callbacks = SCM_UNDEFINED;
static SCM scm_gdb_corba_gdb_callbacks_module = SCM_UNDEFINED;

static SCM scm_gdb_frame_accessors [7];

void
_initialize_stack (void)
{
    SCM scm_gdb_frame_rec;
    gchar *names [7] = { "level", "file", "line", "mid",
                         "pc", "function", "language" };
    static int initialized = 0;
    long i;

    if (initialized)
	return;
    else
	initialized = 1;

    scm_gdb_corba_module_ref = SCM_CDR (scm_intern0 ("module-ref"));
    scm_gdb_corba_module_set_x = SCM_CDR (scm_intern0 ("module-set!"));
    scm_gdb_corba_record_accessor = SCM_CDR (scm_intern0 ("record-accessor"));

    scm_gdb_corba_guile_user = SCM_LIST1 (gh_symbol2scm ("guile-user"));
    scm_gdb_corba_guile_user_module = scm_resolve_module (scm_gdb_corba_guile_user);

    scm_gdb_corba_gdb_callbacks = SCM_LIST2 (gh_symbol2scm ("gdb"),
                                             gh_symbol2scm ("callbacks"));
    scm_gdb_corba_gdb_callbacks_module = scm_resolve_module
	(scm_gdb_corba_gdb_callbacks);

    scm_gdb_frame_rec = scm_apply (scm_gdb_corba_module_ref,
				   SCM_LIST2 (scm_gdb_corba_gdb_callbacks_module,
					      gh_symbol2scm ("gdb-frame-rec")),
				   SCM_EOL);

    scm_protect_object (scm_gdb_corba_module_ref);
    scm_protect_object (scm_gdb_corba_module_set_x);
    scm_protect_object (scm_gdb_corba_record_accessor);
    scm_protect_object (scm_gdb_corba_guile_user);
    scm_protect_object (scm_gdb_corba_guile_user_module);
    scm_protect_object (scm_gdb_corba_gdb_callbacks);
    scm_protect_object (scm_gdb_corba_gdb_callbacks_module);
    scm_protect_object (scm_gdb_frame_rec);

    for (i = 0; i < 7; i++) {
        scm_gdb_frame_accessors [i] =
	    scm_apply (scm_gdb_corba_record_accessor,
		       SCM_LIST2 (scm_gdb_frame_rec,
				  gh_symbol2scm (names [i])),
		       SCM_EOL);
        scm_protect_object (scm_gdb_frame_accessors [i]);
    }
}

void
gdb_corba_get_backtrace (impl_POA_GNOME_Debugger_Program *servant,
                         GNOME_Debugger_Stack *stack, CORBA_Environment *ev)
{
    SCM scm_backtrace;
    long len, i;

    scm_backtrace = scm_gdb_corba_guile_apply
        (servant, scm_ggdb_guile_gdb_corba_backtrace, SCM_EOL, SCM_EOL, ev);

    if (scm_backtrace == SCM_BOOL_F)
        return;

    scm_debug_print (scm_backtrace);

    len = scm_ilength (scm_backtrace);
    if (len < 1)
        return SCM_BOOL_F;

    stack->_buffer = CORBA_sequence_GNOME_Debugger_StackFrame_allocbuf (len);
    stack->_length = stack->_maximum = len;

    for (i = 0; i < len; i++) {
        GNOME_Debugger_StackFrame *frame;
        SCM record, data [7];
        int j;

        record = scm_list_ref (scm_backtrace, SCM_MAKINUM (i));

        scm_debug_print (record);

        for (j = 0; j < 7; j++)
            data [j] = scm_apply (scm_gdb_frame_accessors [j],
                                  SCM_LIST1 (record),
                                  SCM_EOL);

        frame = &(stack->_buffer [i]);
        frame->id = gh_scm2long (data [0]);
        frame->function = gh_scm2newstr (data [5], NULL);
        frame->location.file.filename = gh_scm2newstr (data [1], NULL);
        frame->location.line = gh_scm2long (data [2]);
        frame->virtual_address = gh_scm2long (data [4]);
        frame->intraline = gh_scm2bool (data [3]);
    }

    return SCM_BOOL_T;
}
