/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <gdbguile-corba.h>

SCM_PROC (s_frames_invalid, "gdb-ccb-frames-invalid", 0, 0, 0, sgccb_frames_invalid);

static SCM
sgccb_frames_invalid (void)
{
    SCM s_corba_data = SCM_CDR (scm_ggdb_guile_apply_data);

    SCM_ASSERT (!SCM_UNBNDP (s_corba_data) && SCM_NFALSEP (s_corba_data) &&
                SCM_GGDB_CORBAP (s_corba_data), s_corba_data,
                SCM_ARGn, s_frames_invalid);

    fprintf (stderr, "frames invalid - %p - %p\n",
             SCM_GGDB_CORBA_SERVANT (s_corba_data),
             SCM_GGDB_CORBA_ENV (s_corba_data));

    scm_debug_print (scm_ggdb_guile_apply_data);
    scm_debug_print (scm_ggdb_guile_apply_stack);
    return SCM_BOOL_F;
}

SCM_PROC (s_frame_end, "gdb-ccb-frame-end", 0, 0, 0, sgccb_frame_end);

static SCM
sgccb_frame_end (void)
{
    SCM s_corba_data = SCM_CDR (scm_ggdb_guile_apply_data);

    SCM_ASSERT (!SCM_UNBNDP (s_corba_data) && SCM_NFALSEP (s_corba_data) &&
                SCM_GGDB_CORBAP (s_corba_data), s_corba_data,
                SCM_ARGn, s_frame_end);

    fprintf (stderr, "frame end - %p - %p\n",
             SCM_GGDB_CORBA_SERVANT (s_corba_data),
             SCM_GGDB_CORBA_ENV (s_corba_data));

    scm_debug_print (scm_ggdb_guile_apply_data);
    scm_debug_print (scm_ggdb_guile_apply_stack);

    return SCM_BOOL_F;
}


void
scm_init_gdb_corba_module_callbacks (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "callbacks.x"
#endif
}

void
_initialize_callbacks (void)
{
}
