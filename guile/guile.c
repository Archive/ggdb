/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <gdbguile-corba.h>

SCM_SYMBOL (s_gdb_error, "gdb-error");
SCM_SYMBOL (s_guile_error, "guile-error");
SCM_SYMBOL (s_unknown_error, "unknown-error");

SCM_SYMBOL (s_gdb_corba_run, "gdb-corba-run");
SCM_SYMBOL (s_gdb_corba_backtrace, "gdb-corba-backtrace");

SCM scm_ggdb_guile_module_ref = SCM_UNDEFINED;
SCM scm_ggdb_guile_gdb_corba = SCM_UNDEFINED;
SCM scm_ggdb_guile_gdb_corba_module = SCM_UNDEFINED;

SCM scm_ggdb_guile_gdb_corba_run = SCM_UNDEFINED;
SCM scm_ggdb_guile_gdb_corba_backtrace = SCM_UNDEFINED;

SCM_GLOBAL_VCELL (scm_ggdb_guile_apply_data, "ggdb-guile-apply-data");
SCM_GLOBAL_VCELL (scm_ggdb_guile_apply_stack, "ggdb-guile-apply-stack");

long tc16_ggdb_corba_data;

void
scm_init_gdb_corba_module_guile (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "guile.x"
#endif
}

static scm_sizet
free_ggdb_corba_data (SCM ggdb_corba)
{
    scm_must_free ((char *) SCM_CDR (ggdb_corba));
    return sizeof (ggdb_corba_data);
}

static int
print_ggdb_corba_data (SCM exp, SCM port, scm_print_state *pstate)
{
    scm_puts ("#<ggdb-corba-data ", port);
    scm_intprint (SCM_CDR (exp), 16, port);
    scm_putc ('>', port);
    return 1;
}

void
_initialize_guile (void)
{
    tc16_ggdb_corba_data = scm_make_smob_type_mfpe
        ("gdb-corba-data", sizeof (ggdb_corba_data),
         NULL, free_ggdb_corba_data, print_ggdb_corba_data, NULL);

    scm_ggdb_guile_gdb_corba = scm_permanent_object
        (SCM_LIST2 (gh_symbol2scm ("gdb"), gh_symbol2scm ("corba")));
    scm_ggdb_guile_gdb_corba_module = scm_permanent_object
        (scm_resolve_module (scm_ggdb_guile_gdb_corba));
    scm_ggdb_guile_module_ref = scm_permanent_object
        (SCM_CDR (scm_intern0 ("module-ref")));

    scm_ggdb_guile_gdb_corba_run = scm_permanent_object
        (scm_apply (scm_ggdb_guile_module_ref,
                    SCM_LIST2 (scm_ggdb_guile_gdb_corba_module,
                               s_gdb_corba_run),
                    SCM_EOL));

    scm_ggdb_guile_gdb_corba_backtrace = scm_permanent_object
        (scm_apply (scm_ggdb_guile_module_ref,
                    SCM_LIST2 (scm_ggdb_guile_gdb_corba_module,
                               s_gdb_corba_backtrace),
                    SCM_EOL));
}

typedef struct {
    SCM proc, arg1, args;
    CORBA_Environment *ev;
    char *file, *function;
    unsigned long line;
} guile_dynwind_inner_args;

static void
set_error_location (GNOME_Debugger_ErrorLocation *location,
                    guile_dynwind_inner_args *data)
{
    location->module_name = CORBA_string_dup ("(gdb-guile)");
    location->function = CORBA_string_dup (data->function);
    location->file = CORBA_string_dup (data->file);
    location->line = data->line;
}

static SCM
get_guile_backtrace (const char *function)
{
    SCM the_last_stack = scm_fluid_ref (SCM_CDR (scm_the_last_stack_fluid));
    SCM p;

    if (SCM_FALSEP (the_last_stack))
        the_last_stack = scm_make_stack (scm_cons (SCM_BOOL_T, SCM_EOL));

    p = scm_mkstrport (SCM_INUM0, 
                       scm_make_string (SCM_INUM0, SCM_UNDEFINED),
                       SCM_OPN | SCM_WRTNG, function);

    scm_display_backtrace (the_last_stack, p,
                           SCM_UNDEFINED, SCM_UNDEFINED);
    scm_newline (p);

    return scm_strport_to_string (p);
}

static void
raise_guile_error_exception (guile_dynwind_inner_args *data,
                             SCM tag, SCM message, SCM throw_args)
{
    GNOME_Debugger_GuileError *exception;
    SCM backtrace;

    exception = GNOME_Debugger_GuileError__alloc ();
    exception->message = CORBA_string_dup (gh_scm2newstr (message, NULL));
    exception->tag = CORBA_string_dup (gh_scm2newstr (tag, NULL));
    set_error_location (&exception->location, data);

    scm_backtrace ();

    backtrace = get_guile_backtrace ("scm_gdb_corba_handle_by_exception");

    scm_debug_print (backtrace);

    if (SCM_UNBNDP (backtrace))
        exception->backtrace = CORBA_string_dup ("");
    else
        exception->backtrace = CORBA_string_dup (gh_scm2newstr (backtrace, NULL));

    fprintf (stderr, "CORBA_exception_set: %p - %p - %s\n",
             data->ev, exception, gh_scm2newstr (tag, NULL));

    CORBA_exception_set (data->ev, CORBA_USER_EXCEPTION,
                         ex_GNOME_Debugger_GuileError,
                         exception);
}

static void
raise_gdb_error_exception (guile_dynwind_inner_args *data,
                           SCM tag, SCM message)
{
    GNOME_Debugger_GdbError *exception;

    exception = GNOME_Debugger_GdbError__alloc ();
    exception->message = CORBA_string_dup (gh_scm2newstr (message, NULL));
    set_error_location (&exception->location, data);

    fprintf (stderr, "CORBA_exception_set: %p - %p - %s\n",
             data->ev, exception, gh_scm2newstr (tag, NULL));

    CORBA_exception_set (data->ev, CORBA_USER_EXCEPTION,
                         ex_GNOME_Debugger_GdbError,
                         exception);
}

static void
raise_unknown_error_exception (guile_dynwind_inner_args *data,
                               SCM tag, SCM message)
{
    GNOME_Debugger_UnknownError *exception;

    exception = GNOME_Debugger_UnknownError__alloc ();
    exception->message = CORBA_string_dup (gh_scm2newstr (message, NULL));
    set_error_location (&exception->location, data);

    fprintf (stderr, "CORBA_exception_set: %p - %p - %s\n",
             data->ev, exception, gh_scm2newstr (tag, NULL));

    CORBA_exception_set (data->ev, CORBA_USER_EXCEPTION,
                         ex_GNOME_Debugger_UnknownError,
                         exception);
}

static void
guile_dynwind_before (void *data)
{
    SCM s_data = PTR2SCM (data);

    SCM_SETCDR (scm_ggdb_guile_apply_stack,
                scm_cons (SCM_CDR (scm_ggdb_guile_apply_data),
                          SCM_CDR (scm_ggdb_guile_apply_stack)));
    SCM_SETCDR (scm_ggdb_guile_apply_data, s_data);
}

static void
guile_dynwind_after (void *data)
{
    SCM_SETCDR (scm_ggdb_guile_apply_data,
                SCM_CAR (SCM_CDR (scm_ggdb_guile_apply_stack)));
    SCM_SETCDR (scm_ggdb_guile_apply_stack,
                SCM_CDR (SCM_CDR (scm_ggdb_guile_apply_stack)));
}

static SCM
guile_dynwind_inner_handler (void *p_data, SCM tag, SCM throw_args)
{
    guile_dynwind_inner_args *data = (guile_dynwind_inner_args *) p_data;
    SCM message;
    int len;

    len = scm_ilength (throw_args);

    if (len == 1)
        message = SCM_CAR (throw_args);
    else {
        SCM old_errp, p;

        p = scm_mkstrport (SCM_INUM0, 
                           scm_make_string (SCM_INUM0, SCM_UNDEFINED),
                           SCM_OPN | SCM_WRTNG,
                           "scm_gdb_corba_handle_by_exception");

        old_errp = scm_set_current_error_port (p);

        if (len >= 3)
            scm_handle_by_message_noexit ("scm_gdb_corba_handle_by_exception",
                                          tag, throw_args);
        else
            scm_prin1 (throw_args, p, 1);

        scm_set_current_error_port (old_errp);

        message = scm_strport_to_string (p);
    }

    if (tag == s_gdb_error)
        raise_gdb_error_exception (data, tag, message);
    else if (tag == s_unknown_error)
        raise_unknown_error_exception (data, tag, message);
    else
        raise_guile_error_exception (data, tag, message, throw_args);

    return SCM_BOOL_F;
}

static SCM
guile_dynwind_inner_body (void *p_data)
{
    guile_dynwind_inner_args *data = (guile_dynwind_inner_args *) p_data;

    return scm_apply (data->proc, data->arg1, data->args);
}

static SCM
guile_dynwind_inner (void *p_data)
{
    return scm_internal_stack_catch (SCM_BOOL_T,
                                     guile_dynwind_inner_body, p_data,
                                     guile_dynwind_inner_handler, p_data);
}

SCM
_scm_gdb_corba_guile_apply (impl_POA_GNOME_Debugger_Program *servant,
                            SCM proc, SCM arg1, SCM args,
                            CORBA_Environment *ev,
                            char *function, char *file, unsigned long line)
{
    guile_dynwind_inner_args inner_data = {
        proc, arg1, args, ev, file, function, line
    };
    ggdb_corba_data *data;
    SCM s_data, answer;

    data = (ggdb_corba_data *) scm_must_malloc
        (sizeof (*data), "ggdb-corba-data");
    data->servant = servant;
    data->ev = ev;

    SCM_NEWSMOB (s_data, tc16_ggdb_corba_data, data);

    answer = scm_internal_dynamic_wind (guile_dynwind_before,
                                        guile_dynwind_inner,
                                        guile_dynwind_after,
                                        (void *) &inner_data,
                                        (void *) SCM2PTR (s_data));

    return answer;
}

/**
 * glist_string_from_scm:
 * @guile_list: A guile-list of strings.
 * 
 * Returns a GList of the elements in guile_list.  Assumes all elements in
 * guile_list are strings.
 * 
 * Return value: A GList of malloc()ed strings.  These must be free()ed by the
 * user.
 **/
GList *
glist_string_from_scm (SCM guile_list) 
{
    GList *list_end;
    GList *retval;
    SCM guile_string;
    gchar *str;
    int len;

    retval = NULL;
    list_end = NULL;
    while (guile_list != SCM_EOL) {
        guile_string = SCM_CAR (guile_list);
        str = gh_scm2newstr (guile_string, &len);
        list_end = g_list_append (list_end, str);
        if (retval == NULL) retval = list_end;
        guile_list = SCM_CDR (guile_list);
    }

    return retval;
}


