/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <gdbguile-corba.h>

#include "debugger-program-impl.c"

void
scm_debug_print (SCM obj)
{
    scm_display (obj, scm_current_output_port ());
    scm_newline (scm_current_output_port ());
    scm_flush (scm_current_output_port ());
}

SCM_PROC (s_gdb_corba_init, "gdb-corba-init", 0, 0, 0, sgdb_corba_init);

static SCM
sgdb_corba_init (void)
{
    CORBA_ORB orb;
    CORBA_Environment ev;
    CORBA_Object server;

    PortableServer_POA root_poa;
    PortableServer_POAManager pm;

    static int initialized = 0;

    /* Only initialize once. */
    if (initialized)
        return SCM_BOOL_F;
    initialized = 1;

    /* Initialization functions. */
    _initialize_guile ();
    _initialize_stack ();
    _initialize_callbacks ();

    /* Setup CORBA */
    CORBA_exception_init (&ev);
    
    orb = gnome_CORBA_ORB ();

    root_poa = 
        (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb,
                                                                   "RootPOA",
                                                                   &ev);
    

    server = impl_GNOME_Debugger_Program__create (root_poa, &ev);
    pm = PortableServer_POA__get_the_POAManager (root_poa, &ev);
    PortableServer_POAManager_activate (pm, &ev);

    goad_server_register (CORBA_OBJECT_NIL, server, NULL, NULL, &ev);

    CORBA_exception_free (&ev);
    return SCM_BOOL_T;
}

SCM_PROC (s_gdb_corba_main, "gdb-corba-main", 0, 0, 0, sgdb_corba_main);

static SCM
sgdb_corba_main (void)
{
    sgdb_corba_init ();

    gtk_main ();
    /* will never be reached. */
    return SCM_UNDEFINED;
}

void
scm_init_gdb_corba_module (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "gdbguile-corba.x"
#endif

    scm_init_gdb_corba_module_guile ();
    scm_init_gdb_corba_module_callbacks ();
}
