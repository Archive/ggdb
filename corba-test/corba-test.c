/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
#include <libgnorba/gnorba.h>
#include <gnome-debug/gnome-debug.h>

int 
main (int argc, char * argv[])
{
    GNOME_Debugger_CommanderFactory factory;
    GNOME_Debugger_Commander commander;
    GNOME_Debugger_arg_list *backend_args;

    GNOME_Debugger_CommanderEventSinkFactory client_factory;
    GNOME_Debugger_CommanderEventSink client;

    CORBA_Environment ev;
    CORBA_ORB orb;
  
    CORBA_exception_init (&ev);
    orb = gnome_CORBA_init ("GNOME_Debugger commander client test", "0.1",
			    &argc, argv, 0, &ev);

    factory = goad_server_activate_with_id
	(NULL, "gnome_debugger_commander_factory", 0, NULL);
    
    if (factory == CORBA_OBJECT_NIL) {
        g_error ("Can not bind commander factory");
    }

    backend_args = GNOME_Debugger_arg_list__alloc ();

    commander = GNOME_Debugger_CommanderFactory_create (factory, "gdb-guile",
                                                        backend_args, &ev);

    fprintf (stderr, "Created commander %p via factory %p.\n",
	     commander, factory);

    client_factory = goad_server_activate_with_id
	(NULL, "gnome_debugger_commander_client_factory", 0, NULL);
    
    if (client_factory == CORBA_OBJECT_NIL) {
        g_error ("Can not bind commander client factory");
    }

    client = GNOME_Debugger_CommanderClientFactory_create (client_factory,
                                                           commander, &ev);

    fprintf (stderr, "Created client %p via client factory %p.\n",
	     client, client_factory);

    GNOME_Debugger_Commander_register_client (commander, client, &ev);

    CORBA_exception_free (&ev);
}

